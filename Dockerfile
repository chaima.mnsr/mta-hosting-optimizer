FROM python:3.7

EXPOSE 5000
ENV LIMIT_ACTIVE_IP=1
RUN mkdir /opt/service

WORKDIR /opt/service

COPY . /opt/service/
RUN pip install -r requirements.txt

ENV FLASK_APP=/opt/service/ipconfig/main.py

ENTRYPOINT flask run

