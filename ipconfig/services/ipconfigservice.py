from ipconfig.models.ipconfig import IpConfig


def get_ip_config():
    """
    Function to return all the ip address
    :return: list of IpConfig object
    """
    return [
        IpConfig("127.0.0.1", "mta-prod-1", True),
        IpConfig("127.0.0.2", "mta-prod-1", False),
        IpConfig("127.0.0.3", "mta-prod-2", True),
        IpConfig("127.0.0.4", "mta-prod-2", True),
        IpConfig("127.0.0.5", "mta-prod-2", False),
        IpConfig("127.0.0.6", "mta-prod-3", False)
    ]


def convert_object_to_list():
    """
    Convert list of Object to a list of dictionary
    :return: list of dictionary
    """
    ip_list = []
    for i in get_ip_config():
        ip_list.append({"ip": str(i.ip), "hostname": str(i.hostname), "active": str(i.active)})
    return ip_list


def get_hostname_active_number():
    """
    Function to return the number of active IP address per hostname
    :return: dict where the hostnames are the keys and the value is the number
    of active ip address for that hostname
    """
    ip_dict = {}
    for i in get_ip_config():
        if i.hostname in ip_dict.keys():
            if i.active:
                ip_dict[i.hostname] = ip_dict[i.hostname] + 1
        else:
            if i.active:
                ip_dict[i.hostname] = 1
            else:
                ip_dict[i.hostname] = 0
    return ip_dict


def get_x_active_hostnames(x: int):
    """
    Function to return the hostnames that have a number of active IP less or equal to x
    :param x:
    :return: list of hostnames
    """
    host_list = []
    ip_dict = get_hostname_active_number()
    for i in ip_dict.keys():
        if ip_dict[i] <= x:
            host_list.append(i)
    return host_list
