import os

from flask import Flask

from ipconfig.services.ipconfigservice import get_x_active_hostnames, convert_object_to_list

app = Flask(__name__)
x = int(os.getenv("LIMIT_ACTIVE_IP", 1))


@app.route("/")
def hello():
    return "Welcome Newsletter2Go development test"


@app.route("/ipconfig", methods=['GET'])
def ip_config():
    """
    Get all the IP address, their hostname and their status (active)
    :return: list
    """
    return "here is the list of all the ip addresses their hostname and their status: {0}".format(
        str(convert_object_to_list()))


@app.route("/ipconfig_x", methods=['GET'])
def x_active_hostnames():
    """
    Get hostnames that has a number of active IP address less or equal to x
    :return: result
    """
    return "The hostnames that have active IP address less or equal to {0} are {1}".format(str(x), str(
        get_x_active_hostnames(x)))
