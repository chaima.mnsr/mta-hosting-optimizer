class IpConfig:
    def __init__(self, ip: str, hostname: str, active: bool):
        self.ip = ip
        self.hostname = hostname
        self.active = active

    def __eq__(self, other):
        if not isinstance(other, IpConfig):
            # don't attempt to compare against unrelated types
            return NotImplemented
        return self.ip == other.ip and self.hostname == other.hostname and self.active == other.active

    def __hash__(self):
        # necessary for instances to behave sanely in dicts and sets.
        return hash((self.ip, self.hostname, self.active))
