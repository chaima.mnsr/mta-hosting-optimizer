from ipconfig.models.ipconfig import IpConfig
from ipconfig.services.ipconfigservice import get_ip_config, get_hostname_active_number, get_x_active_hostnames, \
    convert_object_to_list


def test_create_ip_config():
    ip_config = IpConfig("127.0.0.1", "mta-prod-1", True)
    assert ip_config == IpConfig("127.0.0.1", "mta-prod-1", True)


def test_get_ip_config():
    assert get_ip_config() == [
        IpConfig("127.0.0.1", "mta-prod-1", True),
        IpConfig("127.0.0.2", "mta-prod-1", False),
        IpConfig("127.0.0.3", "mta-prod-2", True),
        IpConfig("127.0.0.4", "mta-prod-2", True),
        IpConfig("127.0.0.5", "mta-prod-2", False),
        IpConfig("127.0.0.6", "mta-prod-3", False)
    ]


def test_get_hostname_active_number():
    assert get_hostname_active_number() == {"mta-prod-1": 1, "mta-prod-2": 2, "mta-prod-3": 0}


def test_get_x_active_hostnames():
    assert get_x_active_hostnames(1) == ['mta-prod-1', 'mta-prod-3']


def test_convert_object_to_list():
    assert convert_object_to_list() == [{'ip': '127.0.0.1', 'hostname': 'mta-prod-1', 'active': 'True'},
                                        {'ip': '127.0.0.2', 'hostname': 'mta-prod-1', 'active': 'False'},
                                        {'ip': '127.0.0.3', 'hostname': 'mta-prod-2', 'active': 'True'},
                                        {'ip': '127.0.0.4', 'hostname': 'mta-prod-2', 'active': 'True'},
                                        {'ip': '127.0.0.5', 'hostname': 'mta-prod-2', 'active': 'False'},
                                        {'ip': '127.0.0.6', 'hostname': 'mta-prod-3', 'active': 'False'}]
