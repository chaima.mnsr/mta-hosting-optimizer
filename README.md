# Newsletter2Go development assignment

### Introduction

###### This project is created using `Python 3.7` and `Flask`

This repository presents the development part of Newsletter2Go assignment.
Based on a mock data, this service exposes three endpoints:

* `/`: This endpoint will return a hello message.
* `/ipconfig`: This endpoint will return all the IP addresses present in the sample data, their hostname and their status.
* `/ipconfig_x`: This endpoint will return the list of the hostname that the number of their active IP is less or equal
to X.

By default, `X=1` but this value could be changed using an environment variable as following:

```shell script
export LIMIT_ACTIVE_IP=<value>
```

### Usage

#### Locally

In order to run this project locally you need to:

1- Install requirements:

```shell script
pip install -r requirements.txt
```

2- Set Flask environment variable:

```shell script
export FLASK_APP=ipconfig/main.py
```

3- Run tests:

```shell script
pytest
```

4- See test coverage:

```shell script
coverage run -m pytest
```

5- Set X: (optional, default=1)

```shell script
export LIMIT_ACTIVE_IP=<value>
```

6- Run the application:

```shell script
flask run
```

The application will be running on `http://127.0.0.1:5000`

#### Using docker

In order to run the application using docker:

1- Run the tests:

```shell script
docker-compose up development-ip-config-test
```

2- See the test coverage:

```shell script
docker-compose up development-ip-config-coverage
```

1- Run the application:

* Optional: Change the value of X in `docker-compose.yaml` 

* Run the app using:
```shell script
docker-compose up development-ip-config-app
```

* Access the application on:  `http://127.0.0.1:5000`



